/**
  ******************************************************************************
  * @file	Raingauge_station.c
  * @author CDOH Team
  * @brief  Driver for Rain Gauge with UV Station
  *         This file provides firmware functions to manage the following
  *         functionalities
  *           + Initializes necessary pins for the sensors
  *           + Read data from the sensors
  *           + Maps data to the corresponding value range
  *           + Store Necessary data to the EEPROM
  *@verbatim
  ==============================================================================
                        ##### How to use this driver #####
  ==============================================================================
 	 	 	(+) raingaugeStationInit() to Initializes the Rain Gauge station
 	 	 	(+) readAnalogUvValue() to Read UV data from Rain Gauge station
 	 	 	(+) getAccumulatedRainfall() to Read and store Rain data from Rain Gauge station
 	 	 	(+) readBatteryLevel() to Read Battery status
 **/

/**
 * private Includes
 */

#include <string.h>
#include <stdlib.h>
#include "Weather_Station.h"
#include "adc_if.h"
#include "i2c.h"




/**
  * @brief  Write data to memory
  */
static void writeToMemory(uint32_t Address, uint32_t data);

/**
  * @brief  Read data from memory
  */
static uint16_t readFromMemory(uint32_t Address);

/**
  * @brief  Map ADC value
  */
static uint32_t map(uint32_t au32_IN, uint32_t au32_INmin, uint32_t au32_INmax, uint32_t au32_OUTmin, uint32_t au32_OUTmax);


extern TimerEvent_t InterruptTimer; /*   handler for interrupt timer(wind speed calc)  */

/* user defned variables */

uint16_t rainGaugetipCount = 0;
uint8_t dailyCountFlag = RESET;
uint16_t TotalAccumulatedRainfall=0;
uint16_t windSpeedRotationsCount = 0; /*   Anemometer count */
BMP280_HandleTypedef bme280Sensor1; /*   handler for bme280 sensor  */


/**
  * @brief  read external battery voltage connected to analog channel 4 through a voltage divider
  * @param  none
  * @retval battery voltage level
  */

uint16_t readBatteryLevel(void) {
	int analogValue = 0;		//ADC reading for battery is stored in this variable
	float batteryVoltage = 0;
	uint16_t batteryLevel = 0;

	enable(BATT_POWER);	//Enable battery voltage reading
	HAL_Delay(10);

	analogValue = ADC_ReadChannels(BATTERY_CHANNEL);	//Read battery voltage reading

	disable(BATT_POWER);	//disable battery voltage reading

	batteryVoltage = (analogValue * 3.3 * ((R1 + R2) / R2)) / 4096;	//battery voltage = ADC value*Vref*2/4096   --12 bit ADC with voltage divider factor of 2

	batteryLevel = (uint16_t) (batteryVoltage * 100);	//Multiplication factor of 100 to convert to int from float

	return batteryLevel;
}

/*  getWindDirection  */
/**
 * Created on: Nov 20, 2020
 * Last Edited: Jan 6, 2021
 * Author: Sruthi, Akshay -- ICFOSS
 *
 * @brief  read direction of wind from channel 0 using direction calibrated values
 * @param  none
 * @retval wind direction
 *
 **/
uint16_t getWindDirection() {
	uint16_t analogValue = 0; /*   store the analog value  */
	float direction = 0; /*   actual angle of wind direction functions  */

	float compareDirectionDeg[] = { 112.5, 67.5, 90, 157.5, 135, 202.5, 180,
			22.5, 45, 247.5, 225, 337.5, 0, 292.5, 315, 270 }; /*calibrated values for wind direction readings  */
	uint16_t dirCalibValueMin[] = { 231, 295, 371, 453, 653, 864, 1048, 1482,
			1839, 2266, 2468, 2731, 3010, 3188, 3432, 3734 }; /* calibrated min values  for wind direction*/
	uint16_t dirCalibValueMax[] = { 294, 370, 452, 574, 863, 1001, 1307, 1829,
			2037, 2450, 2729, 2995, 3310, 3413, 3729, 3964 }; /* calibrated max values  for wind direction*/

	/**** {"ESE","ENE","E","SSE","SE","SSW","S","NNE","NE","WSW","SW","NNW","N","WNW","NW","W"} -- directions corresponding to values compareDirectionDeg array  *****/

	/*  power the gpio for reading the analog channel	*/
	enable(WINDDIR_POWER);

	/*	reading analog direction channel	*/
	analogValue = ADC_ReadChannels(ADC_CHANNEL_0);

	/*  disable power after reading the analog channel	*/
	disable(WINDDIR_POWER);

	for (uint8_t i = 0, k = 0; i <= 15; i++, k++) {
		if (analogValue >= dirCalibValueMin[i]
											&& analogValue <= dirCalibValueMax[i]) {
			direction = compareDirectionDeg[k];
		}

	}

	return (uint16_t) (direction * 10);
}


/**
 *
 * @brief initialise a gpio pin in interrupt mode to read every falling edge
 * @note changes in the EXTIx_xx_IRQHandler should be made in order to make the GPIO_Pin act as interrupt
 * @note this pin PA0 is connected to a Rain gauge Read sensor output
 * @param none
 * @retval none
 *
 **/
void rainGaugeInterruptEnable()
{
	GPIO_InitTypeDef  GPIO_Init;

	/* Enable GPIO Clock */

	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_SYSCFG_CLK_ENABLE();
    GPIO_Init.Pin = RAINGAUGE_PIN;
	GPIO_Init.Mode = GPIO_MODE_IT_FALLING;
	GPIO_Init.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(RAINGAUGE_PORT,&GPIO_Init);

	/*Enable and set Rain Interrupt Priority */

	HAL_NVIC_SetPriority((IRQn_Type)EXTI4_15_IRQn,3,0);
    HAL_NVIC_EnableIRQ((IRQn_Type)EXTI4_15_IRQn);
    //APP_PRINTF("Rain_Interrupt_ENable\r\n");
}


/**
 *
 * @brief initialise a gpio pin in interrupt mode to read every falling edge
 * @note changes in the EXTIx_xx_IRQHandler should be made in order to make the GPIO_Pin act as interrupt
 * @note this pin PA0 is connected to a Rain gauge Read sensor output
 * @param none
 * @retval none
 *
 **/
void windSpeedInterruptEnable()
{
	GPIO_InitTypeDef  GPIO_Init;

	/* Enable GPIO Clock */

	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_SYSCFG_CLK_ENABLE();
    GPIO_Init.Pin = WINDSPEED_PIN;
	GPIO_Init.Mode = GPIO_MODE_IT_FALLING;
	GPIO_Init.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(WINDSPEED_PORT,&GPIO_Init);

	/*Enable and set Rain Interrupt Priority */

	HAL_NVIC_SetPriority((IRQn_Type)EXTI4_15_IRQn,3,0);
    HAL_NVIC_EnableIRQ((IRQn_Type)EXTI4_15_IRQn);
    //APP_PRINTF("Rain_Interrupt_ENable\r\n");
}

/**
 *
 * @brief Increases counter variable to measure Rain fall
 * @param none
 * @retval none
 *
 **/

void rainGaugeTips() {
	rainGaugetipCount++;
}

/**
 *
 * @brief get total accumulated rainfall at transmission interval
 * @note after transmission of rainfall the data is reset and starts new count
 * @param none
 * @retval total rainfall at specified interval (sending only the number of tips, it has to be multiplied by the multiplication factor at the decoding end)
 *
 **/
uint16_t getAccumulatedRainfall() {
	uint16_t rainfall = 0;
	rainfall = rainGaugetipCount;
	TotalAccumulatedRainfall += rainfall;
	rainGaugetipCount = 0;
	return (uint16_t) rainfall;

}

/**
 *
 * @brief calculate accumulated rainfall over a period of time (eg: here we get one day data configured using a downlink from server every morning at 8am)
 * @note if downlink received totalrainfall data is reset and starts new count // also writes to memory location
 * @param none
 * @retval totalrainfall at specified interval (sending only the number of tips, it has to be multiplied by the multiplication factor at the decoding end)
 *
 **/

uint16_t getTotalRainfall(uint8_t downlinkReceived){
	uint16_t TotalRainfall=0;

	if(downlinkReceived == RESET){

		TotalRainfall = TotalAccumulatedRainfall;

	}
	else{
		TotalAccumulatedRainfall=0;
		dailyCountFlag = RESET;
	}
	writeToMemory(RAIN_MEMORY_ADD, (uint16_t)TotalRainfall);
	return (uint16_t) TotalRainfall;
}

/**
 *
 * @brief Initializes the necessary pins before reading sensor data
 * @param none
 * @retval none
 *
 **/

void weatherStationGPIO_Init() {
	GPIO_InitTypeDef initStruct = { 0 };
	initStruct.Pull = GPIO_NOPULL;
	initStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	initStruct.Mode = GPIO_MODE_OUTPUT_PP;

	initStruct.Pin = BATT_ENABLE_PIN;
	HAL_GPIO_Init(BATT_ENABLE_PORT, &initStruct);

	initStruct.Pin = WIND_DIRECTION_ENABLE_PIN;
	HAL_GPIO_Init(WIND_DIRECTION_ENABLE_PORT, &initStruct);

	initStruct.Pin = BME280_ENABLE_PIN;
	HAL_GPIO_Init(BME280_ENABLE_PORT, &initStruct);
}

/**
 *
 */

void windSpeedTimerEvent() {
	TimerInit(&InterruptTimer, onWindSpeedTimerEvent);
	TimerSetValue(&InterruptTimer, WS_MEASURMENT_TIME);
}


/*  windSpeedRotations  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief callback function interrupt on PB14 pin
 * @note this pin PB14 is connected to a anemometer Reed sensor
 * @param none
 * @retval none
 *
 **/
void windSpeedRotations(void *context) {
	windSpeedRotationsCount++;
}

/*  getWindSpeed  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief calculate wind speed with number of rotations captured in timer event
 *
 * @param none
 * @retval Wind speed in kmph * 10
 *
 **/
uint16_t getWindSpeed() {
	float windSpeedKMH = 0;

	windSpeedRotationsCount = windSpeedRotationsCount / 2;
	windSpeedKMH = (windSpeedRotationsCount * 2.4) / 5; /* 2.4 kmh for 1 rotation per sec 5 sec*/
	windSpeedRotationsCount = 0;

	return (uint16_t) (windSpeedKMH * 10);
}

/*  onWindSpeedTimerEvent  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 6, 2021
 * Author: Akshay
 *
 * @brief disable the interrupt on PB14 after wind speed timer event expires and set the lorawan transmission.
 *
 * @param none
 * @retval none
 *
 **/
void onWindSpeedTimerEvent() {
	/*	disable PB14 interrupt to timer event of WS_MEASURMENT_TIME*/
	HAL_GPIO_DeInit(WINDSPEED_PORT, WINDSPEED_PIN);
}


/*  bme280HWInit  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 7, 2021
 * Author: Akshay,Sruthi
 *
 * @brief initialise bme280 with default parameters and i2c1
 * @note none
 * @param none
 * @retval none
 *
 **/
void bme280HWInit() {
	bmp280_init_default_params(&bme280Sensor1.params);
	bme280Sensor1.addr = BMP280_I2C_ADDRESS_0;
	bme280Sensor1.i2c = &hi2c1;
}

/*  bme280HWInit  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 7, 2021
 * Author: Akshay,Sruthi
 *
 * @brief initialize the sensor using bmp280_init API and check if sensor is initialized
 * @param none
 * @retval sensor init status
 *
 **/
bool bme280IoInit() {
	/*enable bme280 module power*/
	enable(BME280_POWER);
	HAL_Delay(50);

	bool bme280Status = bmp280_init(&bme280Sensor1, &bme280Sensor1.params);
	HAL_Delay(50);
	if (bme280Status)
		return BME280_INIT_SUCCESS;
	else
		return BME280_INIT_ERROR;
}

/*  readBME280  */
/**
 * Created on: Nov 19, 2020
 * Last Edited: Jan 7, 2021
 * Author: Akshay,Sruthi
 *
 * @brief read data from available API
 * @param sensor data pointer that contains the variables to be read from the sensor
 * @retval none
 *
 **/
void readBME280(awsSensors_t *sensor_data) {

	if (!bmp280_read_float(&bme280Sensor1, &sensor_data->temperature,
			&sensor_data->pressure, &sensor_data->humidity)) {

		sensor_data->temperature = -1;
		sensor_data->pressure = -1;
		sensor_data->humidity = -1;
	}
	disable(BME280_POWER);
}

/**
 *
 * @brief manual control of gpio inorder to enable the power to the sensor
 * @param the gpio to be enabled
 * @retval none
 *
 **/
void enable(uint8_t pin) {

	switch (pin) {
		case 1:
			HAL_GPIO_WritePin(BATT_ENABLE_PORT, BATT_ENABLE_PIN, GPIO_PIN_RESET); //for battery
			break;
		case 2:
			HAL_GPIO_WritePin(WIND_DIRECTION_ENABLE_PORT, WIND_DIRECTION_ENABLE_PIN, GPIO_PIN_SET); //for wind dir power
			break;
		case 3:
			HAL_GPIO_WritePin(BME280_ENABLE_PORT, BME280_ENABLE_PIN, GPIO_PIN_SET); //for bme280 grond enable(mosfet gnd)
			break;
		default:
			break;

}
}

/**
 * @brief manual control of gpio inorder to disable the power to the sensor
 * @param the gpio to be disabled
 * @retval none
 *
 **/
void disable(uint8_t pin) {

	switch (pin) {
		case 1:
			HAL_GPIO_WritePin(BATT_ENABLE_PORT, BATT_ENABLE_PIN, GPIO_PIN_SET); //for battery
			break;
		case 2:
			HAL_GPIO_WritePin(WIND_DIRECTION_ENABLE_PORT, WIND_DIRECTION_ENABLE_PIN, GPIO_PIN_RESET); //for wind dir power
			break;
		case 3:
			HAL_GPIO_WritePin(BME280_ENABLE_PORT, BME280_ENABLE_PIN, GPIO_PIN_RESET); //for bme280 grond enable(mosfet gnd)
			break;
		default:
			break;

}
}

/**
 * @brief Initialize Rain gauge station system
 * @param none
 * @retval none
 *
 **/

void weatherStationInit() {

	MX_I2C1_Init();
	bme280HWInit();
	weatherStationGPIO_Init();
	windSpeedTimerEvent();
	TotalAccumulatedRainfall = readFromMemory(RAIN_MEMORY_ADD);
	writeToMemory(RAIN_MEMORY_ADD, RESET);
}

/**
 *
 * @brief Read Sensor Data
 * @param structure to store the data
 * @retval none
 *
 **/
void readWeatherStationParameters(rainfallData_t *sensor_data) {

	sensor_data->batteryLevel = readBatteryLevel();

	bool bme280InitStatus = bme280IoInit();
	if (bme280InitStatus == BME280_INIT_SUCCESS) {
		readBME280(sensor_data);
	}
	sensor_data->windDirection = getWindDirection();
	sensor_data->rainfall = getAccumulatedRainfall(); // 15 minutes total
	sensor_data->windSpeed = getWindSpeed();

}

/**
 *
 * @brief handles writing data to the memory
 * @param Address location of the data to be stored
 * @param data provides the data to be written
 * @retval none
 *
 **/
static void writeToMemory(uint32_t Address, uint32_t data)
{
	/*EEPROM DATA STORING*/

	HAL_FLASHEx_DATAEEPROM_Unlock();
	HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, Address, data);
	HAL_FLASHEx_DATAEEPROM_Lock();

}

/**
 *
 * @brief handles reading data to from memory
 * @param Address location of the data to be retrieved
 * @retval none
 *
 **/

static uint16_t readFromMemory(uint32_t Address){
	return *(uint32_t *)Address;
}
