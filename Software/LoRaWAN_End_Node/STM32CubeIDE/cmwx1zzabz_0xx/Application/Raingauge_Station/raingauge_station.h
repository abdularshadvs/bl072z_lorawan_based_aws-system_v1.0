/**
  ******************************************************************************
  * @file    raingauge_station.h
  * @author  CDOH Team
  * @brief   This file contains all the functions prototypes for the Rain Gauge station driver.
  ******************************************************************************
  */
#ifndef RAINGAUGE_STATION_H_
#define RAINGAUGE_STATION_H_

#include <stdint.h>
#include <stdlib.h>
#include "sys_app.h"
#include "bmp280.h"

/**
 * @defgroup Battery, Rain Gauge
 */

#define BATT_ENABLE_PORT			GPIOB
#define BATT_ENABLE_PIN				GPIO_PIN_2
#define BATTERY_CHANNEL				ADC_CHANNEL_4


#define RAINGAUGE_PORT				GPIOA
#define RAINGAUGE_PIN				GPIO_PIN_9

#define WINDSPEED_PORT				GPIOB
#define WINDSPEED_PIN				GPIO_PIN_14

#define WIND_DIRECTION_ENABLE_PORT	GPIOB
#define WIND_DIRECTION_ENABLE_PIN	GPIO_PIN_15
#define WIND_DIRECTION_CHANNEL		ADC_CHANNEL_0

#define BATT_POWER    				1
#define WINDDIR_POWER   			2
#define BME280_POWER    			3

#define BME280_ENABLE_PORT			GPIOA
#define BME280_ENABLE_PIN			GPIO_PIN_8
#define BME280_INIT_SUCCESS   		0
#define BME280_INIT_ERROR     		1
#define BME280_READ_FAIL      		1
#define BME280_READ_SUCCESS   		0

#define DOWNLINK_RAINPORT        	5

#define RAIN_MEMORY_ADD				0x08080008


#define R1 	10		/*10 K resistor R1 in voltage divider*/
#define R2 	10		/*10 K resistor R2 in voltage divider*/
#define WS_MEASURMENT_TIME   5000  /* wind speed measurement time 5 sec - defined in ms */
#define WS_RADIUS   0.000072  /* radius in kilometer */

/**
 * Structure to store sensor data
 */
typedef struct {
	float pressure; /* in mbar */
	float temperature; /* in �C   */
	float humidity; /* in %    */
	uint16_t windDirection;
	uint16_t rainfall;
	uint16_t windSpeed;
	uint16_t batteryLevel;

} awsSensors_t;


/** @defgroup Rain Guage Station Exported_Functions_Peripheral Control functions
  * @brief    Peripheral Control functions
  */
uint16_t readBatteryLevel(void);

uint16_t getWindDirection(void);

void windSpeedInterruptEnable();
void windSpeedRotations(void *context);
void windSpeedTimerEvent();
uint16_t getWindSpeed();
void onWindSpeedTimerEvent();


void bme280HWInit();
bool bme280IoInit();
void readBME280(awsSensors_t *sensor_data);

void rainGaugeInterruptEnable();
void rainGaugeTips();
uint16_t getRainfall();
uint16_t getTotalRainfall(uint8_t downlinkReceived);

void enable(uint8_t);
void disable(uint8_t);


void weatherStationInit();
void weatherStationGPIO_Init();
void readWeatherStationParameters(awsSensors_t *sensor_data);

#endif
