################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Application/Weatherstation/Weather_Station.c \
../Application/Weatherstation/i2c.c 

OBJS += \
./Application/Weatherstation/Weather_Station.o \
./Application/Weatherstation/i2c.o 

C_DEPS += \
./Application/Weatherstation/Weather_Station.d \
./Application/Weatherstation/i2c.d 


# Each subdirectory must supply rules for building sources it contributes
Application/Weatherstation/%.o Application/Weatherstation/%.su Application/Weatherstation/%.cyclo: ../Application/Weatherstation/%.c Application/Weatherstation/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DCMWX1ZZABZ0XX -DRAIN -c -I../../../LoRaWAN/App -I"/home/shafeekpm/Videos/Sep14_AWS_v1.0_NewStack_Upgradation/LoRaWAN_End_Node/STM32CubeIDE/cmwx1zzabz_0xx/Application/Weatherstation" -I../../../LoRaWAN/Target -I"/home/shafeekpm/Videos/Sep14_AWS_v1.0_NewStack_Upgradation/LoRaWAN_End_Node/STM32CubeIDE/cmwx1zzabz_0xx/Drivers/BME_280" -I../../../Core/Inc -I../../../Utilities/misc -I../../../Utilities/timer -I../../../Utilities/trace/adv_trace -I../../../Utilities/lpm/tiny_lpm -I../../../Utilities/sequencer -I../../../Drivers/BSP/B-L072Z-LRWAN1 -I../../../Drivers/BSP/CMWX1ZZABZ_0xx -I../../../Drivers/STM32L0xx_HAL_Driver/Inc -I../../../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../../../Drivers/CMSIS/Include -I../../../Middlewares/Third_Party/SubGHz_Phy -I../../../Middlewares/Third_Party/SubGHz_Phy/sx1276 -I../../../Middlewares/Third_Party/LoRaWAN/Crypto -I../../../Middlewares/Third_Party/LoRaWAN/Mac -I../../../Middlewares/Third_Party/LoRaWAN/Mac/Region -I../../../Middlewares/Third_Party/LoRaWAN/Utilities -I../../../Middlewares/Third_Party/LoRaWAN/LmHandler -I../../../Drivers/BSP/IKS01A2 -I../../../Drivers/BSP/Components/Common -I../../../Drivers/BSP/Components/hts221 -I../../../Drivers/BSP/Components/lps22hb -I../../../Drivers/BSP/Components/lsm6dsl -I../../../Drivers/BSP/Components/lsm303agr -I../../../Middlewares/Third_Party/LoRaWAN/LmHandler/Packages -Os -ffunction-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Application-2f-Weatherstation

clean-Application-2f-Weatherstation:
	-$(RM) ./Application/Weatherstation/Weather_Station.cyclo ./Application/Weatherstation/Weather_Station.d ./Application/Weatherstation/Weather_Station.o ./Application/Weatherstation/Weather_Station.su ./Application/Weatherstation/i2c.cyclo ./Application/Weatherstation/i2c.d ./Application/Weatherstation/i2c.o ./Application/Weatherstation/i2c.su

.PHONY: clean-Application-2f-Weatherstation

