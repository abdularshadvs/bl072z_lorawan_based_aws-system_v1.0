################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Drivers/BME_280/bmp280.c 

OBJS += \
./Drivers/BME_280/bmp280.o 

C_DEPS += \
./Drivers/BME_280/bmp280.d 


# Each subdirectory must supply rules for building sources it contributes
Drivers/BME_280/%.o Drivers/BME_280/%.su Drivers/BME_280/%.cyclo: ../Drivers/BME_280/%.c Drivers/BME_280/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m0plus -std=gnu11 -g3 -DSTM32L072xx -DUSE_B_L072Z_LRWAN1 -DCMWX1ZZABZ0XX -DRAIN -c -I../../../LoRaWAN/App -I"/home/shafeekpm/Videos/Sep14_AWS_v1.0_NewStack_Upgradation/LoRaWAN_End_Node/STM32CubeIDE/cmwx1zzabz_0xx/Application/Weatherstation" -I../../../LoRaWAN/Target -I"/home/shafeekpm/Videos/Sep14_AWS_v1.0_NewStack_Upgradation/LoRaWAN_End_Node/STM32CubeIDE/cmwx1zzabz_0xx/Drivers/BME_280" -I../../../Core/Inc -I../../../Utilities/misc -I../../../Utilities/timer -I../../../Utilities/trace/adv_trace -I../../../Utilities/lpm/tiny_lpm -I../../../Utilities/sequencer -I../../../Drivers/BSP/B-L072Z-LRWAN1 -I../../../Drivers/BSP/CMWX1ZZABZ_0xx -I../../../Drivers/STM32L0xx_HAL_Driver/Inc -I../../../Drivers/CMSIS/Device/ST/STM32L0xx/Include -I../../../Drivers/CMSIS/Include -I../../../Middlewares/Third_Party/SubGHz_Phy -I../../../Middlewares/Third_Party/SubGHz_Phy/sx1276 -I../../../Middlewares/Third_Party/LoRaWAN/Crypto -I../../../Middlewares/Third_Party/LoRaWAN/Mac -I../../../Middlewares/Third_Party/LoRaWAN/Mac/Region -I../../../Middlewares/Third_Party/LoRaWAN/Utilities -I../../../Middlewares/Third_Party/LoRaWAN/LmHandler -I../../../Drivers/BSP/IKS01A2 -I../../../Drivers/BSP/Components/Common -I../../../Drivers/BSP/Components/hts221 -I../../../Drivers/BSP/Components/lps22hb -I../../../Drivers/BSP/Components/lsm6dsl -I../../../Drivers/BSP/Components/lsm303agr -I../../../Middlewares/Third_Party/LoRaWAN/LmHandler/Packages -Os -ffunction-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Drivers-2f-BME_280

clean-Drivers-2f-BME_280:
	-$(RM) ./Drivers/BME_280/bmp280.cyclo ./Drivers/BME_280/bmp280.d ./Drivers/BME_280/bmp280.o ./Drivers/BME_280/bmp280.su

.PHONY: clean-Drivers-2f-BME_280

